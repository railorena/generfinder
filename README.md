## Installation

To install geneRFinder, please running the script follow:
``` R
Rscript ./src/config.R
```

## Usage

To run geneRFinder, 
``` R
Rscript ./geneRFinder.R -i [fasta_file_name] -o [output_file_name] -t [thread_number] -s [start_type] -n [intergenic]
``` 

[fasta_file_name]: input file name

[output_file_name]: output file name

[thread_number]: number of thread

[start_type]: type of start codon

   1 - if start codon is ATG
   
   2 - if start codon is ATG, GTG and TTG


[intergenic]: type of sequences

   1 - output without intergenic sequences 

   2 - output with intergenic sequences 

For example,

``` R
Rscript ./geneRFinder.R -i ./example/final.contigs.fa -o output -t 7 -s 1 -n 1
``` 